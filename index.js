const express = require("express");
const { getSharedData, getProtectedData } = require("./data.js");
const { signin, verifyToken } = require("./authController");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post("/api/auth/signin", signin);

app.get("/api/protected", verifyToken, getProtectedData);
app.get("/api/shared", getSharedData);

const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Server listening ${PORT} port`);
});
