const users = require("./users");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const secret = "secret-word";

const signin = (req, res) => {
  const { username, password } = req.body;

  const user = users.find((user) => user.username === username);
  const isPasswordValid = bcrypt.compareSync(password, user.password);

  if (!isPasswordValid) {
    return res.status(401).send({ message: "Wrong password!" });
  }

  const token = jwt.sign({ id: user.id, username }, secret, {
    expiresIn: 60 * 60,
  });

  res.status(200).send({ username, accessToken: token });
};

const verifyToken = (req, res, next) => {
  const token = req.headers.authorization.split(" ")[1];

  if (!token) {
    return res.status(400).send({ message: "No token provided" });
  }

  jwt.verify(token, secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: "Unautorized" });
    }
  });
  next();
};

module.exports = {
  signin,
  verifyToken,
};
