const bcrypt = require('bcrypt')

module.exports = [
    {name:'John', username:'john', password: bcrypt.hashSync('john', 8), id:1},
    {name:'Mark', username:'mark', password: bcrypt.hashSync('mark', 8), id:2},
    {name:'David', username:'david', password: bcrypt.hashSync('david', 8), id:3},
    {name:'Lora', username:'lora', password: bcrypt.hashSync('lora', 8), id:4},
    {name:'Stefany', username:'stefany', password: bcrypt.hashSync('stefany', 8), id:5},
]