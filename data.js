const getProtectedData = (req, res) => {
    return res.status(200).send({ message: "Protected data" });
};

const getSharedData = (req, res) => {
  return res.status(200).send({ message: "Shared data" });
};

module.exports = {
  getProtectedData,
  getSharedData,
};
